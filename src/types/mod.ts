import type { ClusterNodeOptions, DiscordGatewayPayload, NodeOptions } from "../deps.ts";
import { Cluster, Node } from "../deps.ts";
import { SongQueue } from "../queue/mod.ts";
import type { SongQueueEvents } from "../queue/types.ts";

interface commonLavadenoPluginConfig {
  /** Default: false */
  includeQueue?: boolean;
  /** Client name of lavadeno. Shown in lavalink logs. */
  clientName?: string;
  /** Whether to enable filters support. */
  useFilters?: boolean;
  /** Disables the hook for raw event. You will need to call the `bot.lavadeno.handleRawEvent` function manually in your raw event. */
  disableEventHook?: boolean;
}

export type lavadenoPluginNodeConfig = {
  mode: "Node";
} & NodeOptions["connection"] &
  commonLavadenoPluginConfig;

export interface lavadenoPluginClusterConfig extends commonLavadenoPluginConfig {
  mode: "Cluster";
  nodes: ClusterNodeOptions[];
}

//bot.id
export interface lavadenoPlugin<T extends lavadenoPluginNodeConfig | lavadenoPluginClusterConfig, Q extends boolean> {
  lavadeno: {
    core: T extends lavadenoPluginNodeConfig ? Node : Cluster;
    /** botId from discordeno is used. If you want to overwrite it. Do it here. */
    connect(botId?: bigint): Promise<void>;
    handleRawEvent: (data: DiscordGatewayPayload) => Promise<void>;
  } & (Q extends true
    ? {
        queues: Map<bigint, SongQueue>;
        createQueue: (guildId: bigint, textChannelId: bigint, events?: Partial<SongQueueEvents>) => SongQueue;
      }
    : // deno-lint-ignore ban-types
      {});
}

export function isNode(data: Node | Cluster): data is Node {
  return !Reflect.has(data, "nodes");
}

export function isCluster(data: Node | Cluster): data is Cluster {
  return Reflect.has(data, "nodes");
}
