import {
  Bot,
  Cluster,
  constants,
  DiscordGatewayPayload,
  DiscordVoiceServer,
  DiscordVoiceState,
  EventHandlers,
  GatewayManager,
  Node,
  UpdateVoiceStatus,
} from "./deps.ts";
import { SongQueue } from "./queue/mod.ts";
import { SongQueueEvents } from "./queue/types.ts";
import { isNode, lavadenoPlugin, lavadenoPluginClusterConfig, lavadenoPluginNodeConfig } from "./types/mod.ts";
import { assign } from "./utils.ts";

function sendGatewayPayload(gateway: GatewayManager, id: bigint, payload: UpdateVoiceStatus) {
  const shardId = Number(id >> 22n) % gateway.manager.totalShards;
  if (shardId !== undefined) gateway.manager.shards.find((s) => s.id == shardId)?.send(payload);
}

function handleRawEvent(
  // deno-lint-ignore no-explicit-any
  lavadeno: lavadenoPlugin<any, any>["lavadeno"],
  payload: Parameters<EventHandlers["raw"]>[1]
) {
  switch (payload.t) {
    case "VOICE_SERVER_UPDATE":
    case "VOICE_STATE_UPDATE":
      lavadeno.core.handleVoiceUpdate(payload.d as DiscordVoiceState | DiscordVoiceServer);
      break;
  }
}

export function enableLavadenoPlugin<
  T extends Bot /* & CacheProps*/,
  E extends lavadenoPluginClusterConfig | lavadenoPluginNodeConfig
>(bot: T, config: E): T & lavadenoPlugin<E, E extends { includeQueue: true } ? true : false> {
  // Set client name and filters support
  if (config.clientName) constants.clientName = config.clientName;
  if (config.useFilters != undefined) constants.useFilters = config.useFilters;

  assign(bot, {
    lavadeno: {
      connect(botId?: bigint) {
        if (isNode(this.core)) this.core.connect(botId);
        else this.core.init(botId);
      },
      handleRawEvent(data: DiscordGatewayPayload) {
        return handleRawEvent(this, data);
      },
      core:
        config.mode === "Cluster"
          ? new Cluster({
              nodes: config.nodes,
              sendGatewayPayload: (id, payload) => sendGatewayPayload(bot.gateway, id, payload),
              userId: bot.id,
            })
          : new Node({
              connection: {
                host: config.host,
                port: config.port,
                password: config.password,
                secure: config.secure,
                resuming: config.resuming,
                reconnect: config.reconnect,
              },
              sendGatewayPayload: (id, payload) => sendGatewayPayload(bot.gateway, id, payload),
              userId: bot.id,
            }),
    },
  } as lavadenoPlugin<E, E extends { includeQueue: true } ? true : false>);

  if (config.includeQueue) {
    assign(bot.lavadeno, {
      queues: new Map(),
      createQueue: (guildId: bigint, textChannelId: bigint, events?: Partial<SongQueueEvents>) => {
        // deno-lint-ignore no-explicit-any
        return new SongQueue(bot as unknown as Bot & lavadenoPlugin<any, true>, guildId, textChannelId, events);
      },
    });
  }

  if (config.disableEventHook != true) {
    //  Get raw event listener
    const original = bot.events.raw;
    const my: EventHandlers["raw"] = (_, data, shardId) => {
      bot.lavadeno.handleRawEvent(data);
      return original(bot, data, shardId);
    };

    // Overwrite it
    bot.events.raw = my;
  }

  return bot;
}
