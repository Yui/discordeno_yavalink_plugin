import type { Track } from "https://deno.land/x/lavalink_types@2.0.6/mod.ts";
import { songTrackInfo } from "./types.ts";

export class Song implements Track {
  track: Track["track"];
  info: songTrackInfo;
  constructor(track: Track) {
    if (!["soundcloud", "youtube"].includes(track.info.sourceName)) throw new Error("Unknown source");

    this.track = track.track;
    this.info = track.info as songTrackInfo;
  }

  get thumbnail() {
    if (this.info.sourceName === "youtube")
      // Youtube
      return `https://i.ytimg.com/vi/${this.info.identifier}/maxresdefault.jpg`;
    else {
      // Soundcloud
      return ""; // Afaik there is not a easy way to get soundcloud thumbnail without using the api.
    }
  }
}
