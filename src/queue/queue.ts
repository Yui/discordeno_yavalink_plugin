import { Queue } from "https://x.nest.land/Yutils@2.1.0/modules/queue.ts";
import { Bot, Cluster, Player } from "../deps.ts";
import { lavadenoPlugin } from "../types/mod.ts";
import { SongQueueEvents } from "./types.ts";
import { Song } from "./song.ts";

const SongQueueLoopModes = {
  None: undefined,
  Song: 0,
  Queue: 1,
} as const;

// deno-lint-ignore no-explicit-any
export class SongQueue extends Player<any> {
  songs: Queue<Song>;
  events: Partial<SongQueueEvents> = {};
  #loop: typeof SongQueueLoopModes[keyof typeof SongQueueLoopModes];
  #current: Song | undefined;
  #started = false;
  #textChannelId: bigint;
  #skipping = false;
  // deno-lint-ignore no-explicit-any
  bot: Bot & lavadenoPlugin<any, true>;

  constructor(
    // deno-lint-ignore no-explicit-any
    bot: Bot & lavadenoPlugin<any, true>,
    guildId: bigint,
    textChannelId: bigint,
    events?: Partial<SongQueueEvents>
  ) {
    // Create the player
    const node = bot.lavadeno.core instanceof Cluster ? bot.lavadeno.core.idealNodes[0] : bot.lavadeno.core;
    if (!node) throw new Error("No node available");
    super(node, guildId);
    node.players.set(this.guildId, this);
    //

    this.bot = bot;
    this.songs = new Queue();
    this.#textChannelId = textChannelId;
    if (events) this.events = events;

    this.#setupPlayerEvents();

    // Add queue to list
    bot.lavadeno.queues.set(guildId, this);
  }

  get current() {
    return this.#current;
  }

  get textChannelId() {
    return this.#textChannelId;
  }

  get next() {
    return this.songs.peek();
  }

  get size() {
    return this.songs.size;
  }

  get loop(): keyof typeof SongQueueLoopModes {
    if (this.#loop === SongQueueLoopModes.None) return "None";
    else if ((this.#loop = SongQueueLoopModes.Queue)) return "Queue";
    else return "Song";
  }

  set loop(mode: keyof typeof SongQueueLoopModes) {
    this.#loop = SongQueueLoopModes[mode];
  }

  /** Starts the queue */
  async start() {
    if (this.playing && this.#started) return this;

    const song = this.songs.take();
    if (!song) throw new Error("Queue empty");

    this.#started = true;
    this.#current = song;
    await this.play(song);
    return this;
  }

  async skip() {
    if (!this.#current) return;
    this.#skipping = true;
    await this.stop();
    return this;
  }

  add(...songs: Song[]) {
    this.songs.add(songs);
    return this;
  }

  clear() {
    this.songs.clear();
    return this;
  }

  destroy() {
    this.bot.lavadeno.queues.delete(this.guildId);
    this.songs.clear();
    this.off("trackEnd"); // We do not wanna emit this when destroying.
    return super.destroy();
  }

  #setupPlayerEvents() {
    this.on("trackStart", () => {
      this.events.songStart?.(this.#textChannelId, this.#current!);
    }).on("trackEnd", async () => {
      this.events.songEnd?.(this.#textChannelId, this.#current!, this.#skipping);

      // Handle looping
      if (this.#loop != SongQueueLoopModes.None) {
        if (this.#skipping == false && this.#loop == SongQueueLoopModes.Song) {
          await this.play(this.#current!);
        } else {
          // Loop queue
          this.songs.add(this.#current!);

          this.#current = this.songs.take()!;

          await this.play(this.#current!);
        }
      } else {
        const song = this.songs.take();

        if (!song) {
          this.events.finish?.(this.#textChannelId);
          return;
        }

        this.#current = song;
        await this.play(song);
      }

      this.#skipping = false;
    });
  }
}
