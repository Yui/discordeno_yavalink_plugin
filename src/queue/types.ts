import type { TrackInfo } from "https://deno.land/x/lavalink_types@2.0.6/mod.ts";
import { Song } from "./mod.ts";

export interface SongQueueEvents {
  /** channelId is the text channel where queue is bound to. */
  songStart: (channelId: bigint, song: Song) => void;
  /** channelId is the text channel where queue is bound to. */
  songEnd: (channelId: bigint, song: Song, wasSkipped: boolean) => void;
  /** channelId is the text channel where queue is bound to. */
  finish: (channelId: bigint) => void;
}

export interface songTrackInfo extends TrackInfo {
  sourceName: "soundcloud" | "youtube";
}
