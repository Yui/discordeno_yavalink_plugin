import {
  ApplicationCommandOptionChoice,
  Collection,
  CreateApplicationCommand,
  createBot,
  CreateContextApplicationCommand,
  GatewayIntents,
  InteractionCallbackData,
  InteractionResponseTypes,
  startBot,
} from "https://deno.land/x/discordeno@17.0.0/mod.ts";
import { enableCachePlugin, enableCacheSweepers } from "https://deno.land/x/discordeno@17.0.0/plugins/cache/mod.ts";
import { enableLavadenoPlugin } from "../src/mod.ts"; // Import this from nest.land

import { YamlLoader } from "https://deno.land/x/yaml_loader@v0.1.0/mod.ts";
import { Song } from "../src/queue/mod.ts"; // Import this from nest.land

const yoader = new YamlLoader();

const config = (await yoader.parseFile(
  import.meta.url.replace("queue.ts", "config.yaml").replace("file://", "")
  // deno-lint-ignore no-explicit-any
)) as any;

const nodes = [];

for (let i = 0; i < config.lavalink.ids.length; i++) {
  nodes.push({
    id: config.lavalink.ids[i],
    host: config.lavalink.hosts[i],
    port: config.lavalink.ports[i],
    password: config.lavalink.passwords[i],
    secure: config.lavalink.secure[i],
  });
}
if (nodes.length == 0) throw new Error("No nodes specified");

const baseBot = createBot({
  token: config.bottoken,
  intents: GatewayIntents.Guilds | GatewayIntents.GuildVoiceStates,
  botId: BigInt(config.botid),
  events: {
    async interactionCreate(_, interaction) {
      // Defer
      await bot.helpers.sendInteractionResponse(interaction.id, interaction.token, {
        type: InteractionResponseTypes.DeferredChannelMessageWithSource,
      });

      const reply = async (message: string | InteractionCallbackData) =>
        await bot.helpers.sendFollowupMessage(interaction.token, {
          type: InteractionResponseTypes.ChannelMessageWithSource,
          data:
            typeof message === "string"
              ? {
                  content: message,
                }
              : message,
        });

      // This is a very basic example. For proper usage additional checks are needed.
      if (interaction.data?.name === "play") {
        if (!interaction.guildId) return await reply("Not in a guild");

        // Note: voicestates are currently broken in discordeno so this will not work
        const userVoiceChannel = bot.guilds
          .get(interaction.guildId)
          ?.voiceStates?.find((vs) => vs.userId == interaction.user.id);

        if (!userVoiceChannel?.channelId) return await reply("User not in voice channel");

        const song = interaction.data.options![0].value as string;

        let queue = bot.lavadeno.queues.get(interaction.guildId);

        const newQueue = queue == undefined;
        if (!queue) {
          queue = bot.lavadeno.createQueue(interaction.guildId!, interaction.channelId!, {
            songStart: (channelId, song) => {
              bot.helpers
                .sendMessage(channelId, {
                  content: `Playing ${song.info.title}`,
                  components: [
                    {
                      type: 1,
                      components: [
                        {
                          type: 2,
                          label: "Link",
                          style: 5,
                          url: song.info.uri,
                        },
                      ],
                    },
                  ],
                })
                .catch(console.error);
            },
            finish: (channelId) => {
              (async () => {
                await bot.helpers.sendMessage(channelId, { content: `Queue finished` }).catch(console.error);
                await queue?.disconnect().destroy();
              })();
            },
          });
        }

        const tracks = await bot.lavadeno.core.rest.loadTracks(`scsearch:${song}` /*`ytsearch:${song}`*/);

        if (tracks.tracks.length == 0) return await reply("No Songs found");

        if (!queue.connected) await queue.connect(userVoiceChannel?.channelId);

        await queue.add(new Song(tracks.tracks[0]));

        await queue.start();

        await reply(newQueue ? "Playing song" : "Song added to queue");
      } else if (interaction.data?.name === "stop") {
        await bot.lavadeno.queues.get(interaction.guildId!)?.disconnect()?.destroy();
        await reply("Stopped");
      } else if (interaction.data?.name === "info") {
        const queue = bot.lavadeno.queues.get(interaction.guildId!);
        const current = queue?.current;
        await reply({
          content: current
            ? `Currently playing song ${queue?.current.info.title}` + `\nSongs in queue: ${queue.size}`
            : "Currently not playing anything",
        });
      } else if (interaction.data?.name === "skip") {
        await bot.lavadeno.queues.get(interaction.guildId!)?.skip();
        await reply("Skipped");
      } else if (interaction.data?.name === "loop") {
        const mode = interaction.data.options![0].value as "None" | "Song" | "Queue";
        const queue = bot.lavadeno.queues.get(interaction.guildId!);
        if (!queue) return await reply("Not playing");
        queue.loop = mode;
        await reply(`Looping mode set to **${mode}**`);
      } else if (interaction.data?.name === "debug") {
        // You can ignore this
        console.log(new Date(), bot.guilds.find((g) => g.id === interaction.guildId!)?.voiceStates);
        await reply("Debug");
      }
    },
  },
});

const bot = enableLavadenoPlugin(enableCachePlugin(baseBot), {
  mode: "Cluster",
  nodes: nodes,
  includeQueue: true,
});

enableCacheSweepers(bot);

await startBot(bot);

bot.lavadeno.core.on("nodeConnect", (node) => console.log(`Node ${node.id} connected.`));
bot.lavadeno.core.on("nodeDisconnect", (node, code, reason) =>
  console.log(`Node ${node.id} disconnected with code ${code}`, reason)
);
bot.lavadeno.core.on("nodeError", (node, err) => console.error(`Node ${node.id} error`, err));

await bot.lavadeno.connect();

const commands = new Collection<string, CreateApplicationCommand | CreateContextApplicationCommand>()
  .set("play", {
    name: "play",
    description: "play",
    type: 1,
    options: [
      {
        name: "song",
        description: "song name",
        type: 3,
        required: true,
      },
    ],
  })
  .set("stop", {
    name: "stop",
    description: "stops",
    type: 1,
  })
  .set("info", {
    name: "info",
    description: "info",
    type: 1,
  })
  .set("skip", {
    name: "skip",
    description: "skip",
    type: 1,
  })
  .set("loop", {
    name: "loop",
    description: "sets queue loop mode",
    type: 1,
    options: [
      {
        name: "mode",
        description: "which loop mode to set",
        type: 3, // string
        required: true,
        choices: ["None", "Song", "Queue"].map(
          (x) =>
            new Object({
              name: x,
              value: x,
            }) as ApplicationCommandOptionChoice
        ),
      },
    ],
  })
  .set("debug", {
    name: "debug",
    description: "debug",
    type: 1,
  });

const cmdNames = commands.map((val) => val.name);

if (
  (await bot.helpers.getGuildApplicationCommands(config.serverid)).filter((val) => cmdNames.includes(val.name)).size <
  cmdNames.length
)
  await bot.helpers.upsertGuildApplicationCommands(bot.transformers.snowflake(config.serverid), commands.array());
