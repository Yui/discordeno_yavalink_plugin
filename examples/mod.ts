import {
  createBot,
  GatewayIntents,
  InteractionResponseTypes,
  startBot,
} from "https://deno.land/x/discordeno@17.0.0/mod.ts";
import { enableCachePlugin, enableCacheSweepers } from "https://deno.land/x/discordeno@17.0.0/plugins/cache/mod.ts";
import { enableLavadenoPlugin } from "../src/mod.ts"; // Import this from nest.land

import { YamlLoader } from "https://deno.land/x/yaml_loader@v0.1.0/mod.ts";

const yoader = new YamlLoader();

// deno-lint-ignore no-explicit-any
const config = (await yoader.parseFile(import.meta.url.replace("mod.ts", "config.yaml").replace("file://", ""))) as any;

const nodes = [];

for (let i = 0; i < config.lavalink.ids.length; i++) {
  nodes.push({
    id: config.lavalink.ids[i],
    host: config.lavalink.hosts[i],
    port: config.lavalink.ports[i],
    password: config.lavalink.passwords[i],
    secure: config.lavalink.secure[i],
  });
}
if (nodes.length == 0) throw new Error("No nodes specified");

const baseBot = createBot({
  token: config.bottoken,
  intents: GatewayIntents.Guilds | GatewayIntents.GuildVoiceStates,
  botId: BigInt(config.botid),
  events: {
    async interactionCreate(_, interaction) {
      // Defer
      await bot.helpers.sendInteractionResponse(interaction.id, interaction.token, {
        type: InteractionResponseTypes.DeferredChannelMessageWithSource,
      });

      // This is a very basic example. For proper usage additional checks are needed.
      if (interaction.data?.name === "play") {
        const reply = async (message: string) =>
          await bot.helpers.sendInteractionResponse(interaction.id, interaction.token, {
            type: InteractionResponseTypes.ChannelMessageWithSource,
            data: {
              content: message,
            },
          });

        if (!interaction.guildId) return await reply("Not in a guild");

        // Note: voicestates are currently broken in discordeno so this will not work
        const userVoiceChannel = bot.guilds
          .get(interaction.guildId)
          ?.voiceStates?.find((vs) => vs.userId == interaction.user.id);

        if (!userVoiceChannel?.channelId) return await reply("User not in voice channel");

        const song = interaction.data.options![0].value as string;

        const player = bot.lavadeno.core.createPlayer(interaction.guildId);

        const tracks = await bot.lavadeno.core.rest.loadTracks(`scsearch:${song}`);

        if (tracks.tracks.length == 0) return await reply("No Songs found");

        await player.connect(userVoiceChannel?.channelId);

        await player.play(tracks.tracks[0].track);
      }
    },
  },
});

const bot = enableLavadenoPlugin(enableCachePlugin(baseBot), {
  mode: "Cluster",
  nodes: nodes,
});

enableCacheSweepers(bot);

await startBot(bot);

bot.lavadeno.core.on("nodeConnect", (node) => console.log(`Node ${node.id} connected.`));
bot.lavadeno.core.on("nodeDisconnect", (node, code, reason) =>
  console.log(`Node ${node.id} disconnected with code ${code}`, reason)
);
bot.lavadeno.core.on("nodeError", (node, err) => console.error(`Node ${node.id} error`, err));

await bot.lavadeno.connect();

if (
  !(await bot.helpers.getGuildApplicationCommands(bot.transformers.snowflake(config.serverid))).find(
    (c) => c.name === "play"
  )
)
  await bot.helpers.upsertGuildApplicationCommands(bot.transformers.snowflake(config.serverid), [
    {
      name: "play",
      description: "play",
      type: 1,
      options: [
        {
          name: "song",
          description: "song name",
          type: 3,
          required: true,
        },
      ],
    },
  ]);
